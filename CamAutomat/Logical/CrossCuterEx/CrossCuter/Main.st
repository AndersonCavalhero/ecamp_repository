
PROGRAM _INIT
	(* Insert code here *)
	MpAxisBasic_Master.MpLink 			:= ADR(gAxisBasicMaster);
	MpAxisBasic_Master.Parameters		:= ADR(MasterPars);
	MpAxisBasic_Master.Axis				:= ADR(gAxis01);
	MpAxisBasic_Master.Enable 			:= TRUE;

	MpAxisBasic_Slave.MpLink 			:= ADR(gAxisBasicSlave);
	MpAxisBasic_Slave.Parameters		:= ADR(SlavePars);
	MpAxisBasic_Slave.Axis				:= ADR(gAxis02);
	MpAxisBasic_Slave.Enable			:= TRUE;
	
	MpAxisCamSequencer_0.MpLink			:= ADR(gAxisBasicSlave);
	MpAxisCamSequencer_0.Enable			:= TRUE;
	MpAxisCamSequencer_0.MpLinkMaster 	:= ADR(gAxisBasicMaster);
	MpAxisCamSequencer_0.Parameters 	:= ADR(CamPars);
	//Master parameters
	MasterPars.Velocity 				:= 100;
	MasterPars.Acceleration				:= 500;
	
	MasterPars.Home.Mode				:= mcHOME_REF_PULSE;
	SlavePars.Home.Mode					:= mcHOME_REF_PULSE;
	MasterPars.Home.ReferencePulse		:= mpAXIS_HOME_OPTION_ON;
	SlavePars.Home.ReferencePulse		:= mpAXIS_HOME_OPTION_ON;
	
	MasterPars.Home.HomingVelocity		:= 50;
	SlavePars.Home.HomingVelocity		:= 50;
	
	//initialization of the masterpars to start the cam
	CamPars.Configuration.StartPosition			:= 0;
	CamPars.Configuration.StartInterval			:= 1;
	CamPars.Configuration.StartState			:= 0;
	CamPars.Configuration.MaxMasterVelocity 	:= 720;
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	CASE Step OF	
		ST_WAIT:
			IF (MpAxisBasic_Master.Info.ReadyToPowerOn AND MpAxisBasic_Slave.Info.ReadyToPowerOn) THEN		
				Step := ST_POWER;	  	
			ELSIF (MpAxisBasic_Master.PowerOn) THEN	
				Step := ST_HOME;
		    END_IF;		

		ST_POWER:
			MpAxisBasic_Master.Power		:= TRUE;
			MpAxisBasic_Slave.Power			:= TRUE;
			IF (MpAxisBasic_Master.PowerOn AND MpAxisBasic_Slave.PowerOn )THEN
				Step := ST_HOME;
			END_IF;
			
		ST_HOME:
			MasterPars.Home.Position   		:=  MASTER_COMPENSATION;//Compensation 
			SlavePars.Home.Position   		:=  SLAVE_COMPENSATION;//Compensation 
			MpAxisBasic_Master.Home			:= TRUE;
			MpAxisBasic_Slave.Home			:= TRUE;
			
			IF (MpAxisBasic_Master.IsHomed AND MpAxisBasic_Slave.IsHomed) THEN
				MpAxisBasic_Master.Home		:= FALSE;
				MpAxisBasic_Slave.Home		:= FALSE;
	  			Step := ST_MOVE_TO_ZERO;
			END_IF;
			
		ST_MOVE_TO_ZERO:
			MasterPars.Position						:= 0;
			SlavePars.Position						:= 0;
			MpAxisBasic_Master.MoveAbsolute			:= TRUE;
			MpAxisBasic_Slave.MoveAbsolute			:= TRUE;
			
			IF (MpAxisBasic_Master.Info.MoveDone AND MpAxisBasic_Slave.Info.MoveDone ) THEN
				MpAxisBasic_Master.MoveAbsolute			:= FALSE;
				MpAxisBasic_Slave.MoveAbsolute			:= FALSE;
				Step := ST_MOVE;
			END_IF;

		ST_MOVE:
			MpAxisBasic_Master.MoveVelocity	:= TRUE;
			IF (MpAxisBasic_Master.InVelocity) THEN
				Step := ST_INIT_AUT;
			END_IF;
			
		ST_INIT_AUT:
			MpAxisCamSequencer_0.StartSequence	:= TRUE;
			MpAxisCamSequencer_0.Update			:= TRUE;
			
			//State 0
			CamPars.Configuration.State[0].Event[0].Type		:= ncS_START;
			CamPars.Configuration.State[0].Event[0].Attribute   := ncAT_ONCE;
			CamPars.Configuration.State[0].Event[0].NextState	:= 1;
			//State 1
			CamPars.Configuration.State[1].CamProfileIndex  := 16#FFFF;
			CamPars.Configuration.State[1].MasterFactor	 	:= 1000;
			CamPars.Configuration.State[1].SlaveFactor      := 0;
			CamPars.Configuration.State[1].CompMode         := ncOFF;
		
			// Event 0 in state 1
				CamPars.Configuration.State[1].Event[0].Type      := ncSIGNAL1;
				CamPars.Configuration.State[1].Event[0].Attribute := ncAT_ONCE;
				CamPars.Configuration.State[1].Event[0].NextState := 1;
				// Event 1 inside the state 1
				CamPars.Configuration.State[1].Event[1].Type		:= ncST_END;
				CamPars.Configuration.State[1].Event[1].Attribute  := ncST_END;
				CamPars.Configuration.State[1].Event[1].NextState  := 1;
			
			
//			//State 2
//			gCamPars.Configuration.State[1].CamProfileIndex  = 0xFFFF;
//			gCamPars.Configuration.State[1].MasterFactor	 = 2000;
//			gCamPars.Configuration.State[1].SlaveFactor      = 0;
//			gCamPars.Configuration.State[1].CompMode         = ncONLYCOMP;
//			gCamPars.Configuration.State[1].MasterCompDistance = 340;
//			gCamPars.Configuration.State[1].SlaveCompDistance  = 0;
//			// Event 0 in state 1
//			gCamPars.Configuration.State[1].Event[0].Type      = ncS_START;
//			gCamPars.Configuration.State[1].Event[0].Attribute = ncST_END;
//			gCamPars.Configuration.State[1].Event[0].NextState = 2;
//			// Event 1 inside the state 1
//			gCamPars.Configuration.State[1].Event[1].Type		= ncST_END;
//			gCamPars.Configuration.State[1].Event[1].Attribute  = ncST_END;
//			gCamPars.Configuration.State[1].Event[1].NextState  = 1;
			
		ST_READY:				 	 	 	 

	END_CASE;
	MpAxisBasic_Master();
	MpAxisBasic_Slave();
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)	
	MpAxisBasic_Master.Enable := FALSE;
	MpAxisBasic_Slave.Enable := FALSE;

	MpAxisBasic_Master();
	MpAxisBasic_Slave();
END_PROGRAM

