
TYPE
	Enum_type : 
		(
		ST_WAIT := 0,
		ST_POWER := 1,
		ST_HOME := 2,
		ST_MOVE_TO_ZERO := 3,
		ST_MOVE := 4,
		ST_INIT_AUT := 5,
		ST_READY := 6
		);
END_TYPE
