
#include <bur/plctypes.h>


#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	//master
	MpAxisBasic_Master.MpLink = (UDINT)&gAxisBasicMaster ;
	MpAxisBasic_Master.Enable = 1;
	MpAxisBasic_Master.Parameters =(UDINT)&MasterPars ;
	MpAxisBasic_Master.Axis = (UDINT)&gAxis01 ;
	// slave
	MpAxisBasic_Slave.MpLink = (UDINT)&gAxisBasicSlave ;
	MpAxisBasic_Slave.Enable = 1;
	MpAxisBasic_Slave.Parameters =(UDINT)&SlavePars ;
	MpAxisBasic_Slave.Axis = (UDINT)&gAxis02 ;
// Automat
	MpAxisCamSequencer_0.MpLink	= (UDINT)&gAxisBasicSlave;
	MpAxisCamSequencer_0.Enable	= 1;

	MpAxisCamSequencer_0.Parameters	= (UDINT)&gCamPars;
	MpAxisCamSequencer_0.MpLinkMaster = (UDINT)&gAxisBasicMaster;
// Automat Parameters
	gCamPars.Configuration.Master	= (UDINT)&gAxis01;
	gCamPars.Configuration.StartPosition	= 10;
	gCamPars.Configuration.StartInterval    = 360; // espera para iniciar de verdadeo nc_START
	gCamPars.Configuration.StartState       = 0;
	gCamPars.Configuration.MaxMasterVelocity= 720;	
	
	MasterPars.Velocity			= 100;
	MasterPars.Acceleration		= 500;
		
}

void _CYCLIC ProgramCyclic(void)
{

	switch (Step)
	{
		case ST_WAIT:
			if (MpAxisBasic_Master.Info.ReadyToPowerOn && MpAxisBasic_Slave.Info.ReadyToPowerOn)
			{
				Step = ST_POWER;	  
			}
			else if (MpAxisBasic_Master.PowerOn)
			{
				Step = ST_HOME;
			}
			break;
		case ST_POWER:
			MpAxisBasic_Master.Power		= 1;
			MpAxisBasic_Slave.Power			= 1;
			if (MpAxisBasic_Master.PowerOn && MpAxisBasic_Slave.PowerOn )
			{
				Step = ST_HOME;
			}
			break;
		case ST_HOME:
		
			MpAxisBasic_Master.Home		= 1;
			MpAxisBasic_Slave.Home		= 1;
			
			if (MpAxisBasic_Master.IsHomed && MpAxisBasic_Slave.IsHomed)
			{
				MpAxisBasic_Master.Home		= 0;
				MpAxisBasic_Slave.Home		= 0;
	  			Step = ST_MOVE;
			}
			break;
		case ST_MOVE:
			MpAxisBasic_Master.MoveVelocity	= 1;
			if (MpAxisBasic_Master.InVelocity)
			{
				Step = ST_INIT_AUT;
			}
			break;
		case ST_INIT_AUT:
			MpAxisCamSequencer_0.StartSequence	= 1;
			MpAxisCamSequencer_0.Update		= 1;
		
			// state 0
			gCamPars.Configuration.State[0].Event[0].Type	= ncS_START;
			gCamPars.Configuration.State[0].Event[0].Attribute = ncAT_ONCE;
			gCamPars.Configuration.State[0].Event[0].NextState = 1;
			//state 1
			gCamPars.Configuration.State[1].CamProfileIndex  = 0xFFFF;
			gCamPars.Configuration.State[1].MasterFactor	 = 2000;
			gCamPars.Configuration.State[1].SlaveFactor      = 0;
			gCamPars.Configuration.State[1].CompMode         = ncONLYCOMP;
			gCamPars.Configuration.State[1].MasterCompDistance = 340;
			gCamPars.Configuration.State[1].SlaveCompDistance  = 0;
				 // Event 0 in state 1
				gCamPars.Configuration.State[1].Event[0].Type      = ncS_START;
				gCamPars.Configuration.State[1].Event[0].Attribute = ncST_END;
				gCamPars.Configuration.State[1].Event[0].NextState = 2;
				// Event 1 inside the state 1
				gCamPars.Configuration.State[1].Event[1].Type		= ncST_END;
				gCamPars.Configuration.State[1].Event[1].Attribute  = ncST_END;
				gCamPars.Configuration.State[1].Event[1].NextState  = 1;
			
			// State 2 - First Label
			gCamPars.Configuration.State[2].CamProfileIndex           =    0xFFFF;
			gCamPars.Configuration.State[2].MasterFactor          	  =    5000;
			gCamPars.Configuration.State[2].SlaveFactor               =    5000;
			gCamPars.Configuration.State[2].CompMode                  =    ncONLYCOMP;
			gCamPars.Configuration.State[2].MasterCompDistance        =    310;
			gCamPars.Configuration.State[2].SlaveCompDistance         =    130;
            
			// State 2, Event 0
//				gCamPars.Configuration.State[2].Event[0].Type         =    ncSIGNAL1;
//				gCamPars.Configuration.State[2].Event[0].Attribute    =    ncST_END;
//				gCamPars.Configuration.State[2].Event[0].NextState    =    3;
	                
				// State 2, Event 1
				gCamPars.Configuration.State[2].Event[1].Type         =    ncST_END;
				gCamPars.Configuration.State[2].Event[1].Attribute    =    ncST_END;
				gCamPars.Configuration.State[2].Event[1].NextState    =    2;
	            
//			// State 3 - Repeat Label
//			gCamPars.Configuration.State[3].CamProfileIndex           =    0xFFFF;
//			gCamPars.Configuration.State[3].MasterFactor              =    2000;
//			gCamPars.Configuration.State[3].SlaveFactor               =    2000;
//			gCamPars.Configuration.State[3].CompMode                  =    ncWITH_CAM;
//			gCamPars.Configuration.State[3].MasterCompDistance        =    360;
//			gCamPars.Configuration.State[3].SlaveCompDistance         =    360;        
//            
//				// State 3, Event 0
//				gCamPars.Configuration.State[3].Event[0].Type         =    ncST_END;
//				gCamPars.Configuration.State[3].Event[0].Attribute    =    ncST_END;
//				gCamPars.Configuration.State[3].Event[0].NextState    =    4;
//	                
//				// State 3, Event 1
//				gCamPars.Configuration.State[3].Event[1].Type         =    ncSIGNAL1;
//				gCamPars.Configuration.State[3].Event[1].Attribute    =    ncST_END;
//				gCamPars.Configuration.State[3].Event[1].NextState    =    3;
//            
//			// State 4 - Stopping
//			gCamPars.Configuration.State[4].CamProfileIndex           =    0xFFFF;
//			gCamPars.Configuration.State[4].MasterFactor              =    2000;
//			gCamPars.Configuration.State[4].SlaveFactor               =    0;
//			gCamPars.Configuration.State[4].CompMode                  =    ncWITH_CAM;
//			gCamPars.Configuration.State[4].MasterCompDistance        =    360;
//			gCamPars.Configuration.State[4].SlaveCompDistance         =    360;            // 18000?
//            
//				// State 4, Event 0
//				gCamPars.Configuration.State[4].Event[0].Type         =    ncSIGNAL1;
//				gCamPars.Configuration.State[4].Event[0].Attribute    =    ncST_END;
//				gCamPars.Configuration.State[4].Event[0].NextState    =    2;
//	                
//				// State 4, Event 1
//				gCamPars.Configuration.State[4].Event[1].Type         =    ncST_END;
//				gCamPars.Configuration.State[4].Event[1].Attribute    =    ncST_END;
//				gCamPars.Configuration.State[4].Event[1].NextState    =    2;
//	             
//			if (MpAxisCamSequencer_0.UpdateDone)
//			{
//				MpAxisCamSequencer_0.Update = 0;
//				Step = ST_READY;
//			}
//			break;
		case ST_READY:
        
			if (SignalOld == 1 && MpAxisCamSequencer_0.Info.ActiveSignal1 == 0)
			{
				MpAxisCamSequencer_0.Signal1 = 1;
			}
            
			SignalOld = MpAxisCamSequencer_0.Info.ActiveSignal1;
            
			break;

	}
     
	MpAxisBasic(&MpAxisBasic_Master);
	MpAxisBasic(&MpAxisBasic_Slave);
	MpAxisCamSequencer(&MpAxisCamSequencer_0);
}

void _EXIT ProgramExit(void)
{
	
	MpAxisBasic_Master.Enable = 0;
	MpAxisBasic_Slave.Enable  = 0;
	MpAxisCamSequencer_0.Enable = 0;
	MpAxisCamSequencer(&MpAxisCamSequencer_0);
	MpAxisBasic(&MpAxisBasic_Master);
	MpAxisBasic(&MpAxisBasic_Slave);
}

