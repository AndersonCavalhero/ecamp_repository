
PROGRAM _INIT
	(* Insert code here *)
	MpAxisBasic_Master.MpLink := ADR(gAxisBasicMaster);
	MpAxisBasic_Master.Parameters	:= ADR(MasterPars);
	MpAxisBasic_Master.Axis	:=	 ADR(gAxis01);
	MpAxisBasic_Master.Enable := TRUE;
	MpAxisCoupling_0.MpLink := ADR(gAxisBasicSlave);
	MpAxisCoupling_0.MpLinkMaster := ADR(gAxisBasicMaster);
	MpAxisCoupling_0.Parameters := ADR(CouplePars);
	
	MpAxisCoupling_0.Enable	:= TRUE;
	
	MpAxisBasic_Slave.MpLink := ADR(gAxisBasicSlave);
	MpAxisBasic_Slave.Parameters	:= ADR(SlavePars);
	MpAxisBasic_Slave.Axis	:=	 ADR(gAxis02);
	MpAxisBasic_Slave.Enable := TRUE;
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	MpAxisCoupling_0();
	MpAxisBasic_Master();
	MpAxisBasic_Slave();
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)	
	MpAxisCoupling_0.Enable := FALSE;
	MpAxisBasic_Master.Enable := FALSE;
	MpAxisBasic_Slave.Enable := FALSE;
	MpAxisCoupling_0();
	MpAxisBasic_Master();
	MpAxisBasic_Slave();
END_PROGRAM

