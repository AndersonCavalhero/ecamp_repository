/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1614088613_2_
#define _BUR_1614088613_2_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL unsigned char MASTER_COMPENSATION;
_GLOBAL unsigned char SLAVE_COMPENSATION;
_GLOBAL struct MpAxisCamSequencerParType gSlavePars;
_GLOBAL struct MpAxisCamSequencerParType gCamPars;
_GLOBAL struct ACP10AXIS_typ gAxis02;
_GLOBAL struct ACP10AXIS_typ gAxis01;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1614088613_2_ */

