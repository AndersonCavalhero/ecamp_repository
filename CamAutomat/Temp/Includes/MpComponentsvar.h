/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _BUR_1614088613_3_
#define _BUR_1614088613_3_

#include <bur/plctypes.h>

/* Constants */
#ifdef _REPLACE_CONST
#else
#endif


/* Variables */
_GLOBAL struct MpComIdentType gAxisBasicSlave;
_GLOBAL struct MpComIdentType gAxisBasicMaster;





__asm__(".section \".plc\"");

/* Used IEC files */
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/X20CP1585/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");

/* Exported library functions and function blocks */

__asm__(".previous");


#endif /* _BUR_1614088613_3_ */

