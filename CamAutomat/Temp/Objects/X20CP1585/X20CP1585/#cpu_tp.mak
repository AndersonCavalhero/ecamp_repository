SHELL := cmd.exe

export AS_SYSTEM_PATH := C:/BrAutomation/AS/System
export AS_BIN_PATH := C:/BrAutomation/AS48/Bin-en
export AS_INSTALL_PATH := C:/BrAutomation/AS48
export AS_PATH := C:/BrAutomation/AS48
export AS_VC_PATH := C:/BrAutomation/AS48/AS/VC
export AS_GNU_INST_PATH := C:/BrAutomation/AS48/AS/GnuInst/V4.1.2
export AS_STATIC_ARCHIVES_PATH := C:/Users/cavalheroa/Desktop/E-camp\ 2021/Motion/CamAutomat/Temp/Archives/X20CP1585/X20CP1585
export AS_CPU_PATH := C:/Users/cavalheroa/Desktop/E-camp\ 2021/Motion/CamAutomat/Temp/Objects/X20CP1585/X20CP1585
export AS_CPU_PATH_2 := C:/Users/cavalheroa/Desktop/E-camp 2021/Motion/CamAutomat/Temp/Objects/X20CP1585/X20CP1585
export AS_TEMP_PATH := C:/Users/cavalheroa/Desktop/E-camp\ 2021/Motion/CamAutomat/Temp
export AS_BINARIES_PATH := C:/Users/cavalheroa/Desktop/E-camp\ 2021/Motion/CamAutomat/Binaries
export AS_PROJECT_CPU_PATH := C:/Users/cavalheroa/Desktop/E-camp\ 2021/Motion/CamAutomat/Physical/X20CP1585/X20CP1585
export AS_PROJECT_CONFIG_PATH := C:/Users/cavalheroa/Desktop/E-camp\ 2021/Motion/CamAutomat/Physical/X20CP1585
export AS_PROJECT_PATH := C:/Users/cavalheroa/Desktop/E-camp\ 2021/Motion/CamAutomat
export AS_PROJECT_NAME := CamAutomat
export AS_PLC := X20CP1585
export AS_TEMP_PLC := X20CP1585
export AS_USER_NAME := cavalheroa
export AS_CONFIGURATION := X20CP1585
export AS_COMPANY_NAME := B&R\ Industrial\ Automation\ GmbH
export AS_VERSION := 4.8.3.111\ SP


default: \



